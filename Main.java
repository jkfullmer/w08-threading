/*
Name: W08 Threading
Scenario: If I were a computer, I could work on several homework assignments at once!
Author: Joshua Fullmer
Class: CIT 360
 */

package com.company;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {

        // Thread pool to execute. Cannot handle more than 4 assignments at a time.
        ExecutorService assignment_studier = Executors.newFixedThreadPool(4);
        // List of assignments for the next couple of days.
        RunnableThread assignment1 = new RunnableThread("OSII", "Test Out", 180);
        RunnableThread assignment2 = new RunnableThread("DA", "Scenario", 90);
        RunnableThread assignment3 = new RunnableThread("OSII", "Group Work", 60);
        RunnableThread assignment4 = new RunnableThread("PBB", "Group Work", 60);
        RunnableThread assignment5 = new RunnableThread("PM", "Discussion Board", 20);
        RunnableThread assignment6 = new RunnableThread("OOD", "Topic", 210);
        RunnableThread assignment7 = new RunnableThread("OOD", "Journal", 15);
        RunnableThread assignment8 = new RunnableThread("PBB", "Program", 60);
        RunnableThread assignment9 = new RunnableThread("PBB", "Checkpoint", 30);
        RunnableThread assignment10 = new RunnableThread("PM", "Project Work", 180);

        // Start the assignments
        assignment_studier.execute(assignment1);
        assignment_studier.execute(assignment2);
        assignment_studier.execute(assignment3);
        assignment_studier.execute(assignment4);
        assignment_studier.execute(assignment5);
        assignment_studier.execute(assignment6);
        assignment_studier.execute(assignment7);
        assignment_studier.execute(assignment8);
        assignment_studier.execute(assignment9);
        assignment_studier.execute(assignment10);

        // Shut it down!
        assignment_studier.shutdown();
    }
}