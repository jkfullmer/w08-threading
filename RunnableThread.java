package com.company;

public class RunnableThread implements Runnable {
    private String class_id;
    private String assignment;
    int length;

    public RunnableThread(String class_id, String assignment, int length) {
        // Creates an assignment with a class_id and a length of time.
        this.class_id = class_id;
        this.assignment = assignment;
        this.length = length; // Represents minutes
    }

    public void run() {
        System.out.println("Work on " + assignment + " for " + class_id + " has begun.");
        for (int i = 1; i < length; i++) {
            //System.out.print("Busy with " + class_id + " " + assignment + "; ");
            try {
                Thread.sleep(length);
            } catch (InterruptedException ie) {
                System.out.println(ie);
            }
        }
        System.out.println("DONE! The " + assignment + " assignment for " + class_id + " is complete. It took " + length + " minutes.");
    }
}